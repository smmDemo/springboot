## create project
https://start.spring.io/

## Spring Boot + Thymeleaf + Maven Example
http://localhost:8080/app/create-user

## SpringBoot + h2 + mybatisでHelloWorld
https://naosim.hatenablog.jp/entry/2018/11/28/074404

## mybatis-spring-boot-starterの使い方
https://qiita.com/kazuki43zoo/items/ea79e206d7c2e990e478

## Spring Boot で Thymeleaf 使い方メモ
https://qiita.com/opengl-8080/items/eb3bf3b5301bae398cc2

## Spring BootでMockitoでモックテスト
http://pppurple.hatenablog.com/entry/2016/11/09/044436

## Spring Boot + JUnit + Mockitoで単体テストをやる
https://qiita.com/YutaKase6/items/1f3ca15900b5146351de

## Spring Batch Unit Testing and Mockito
https://keyholesoftware.com/2012/02/23/spring-batch-unit-testing-and-mockito/