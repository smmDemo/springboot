package com.servicememe.springbootdemo.service;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

import com.servicememe.springbootdemo.domain.User;
import com.servicememe.springbootdemo.mapper.UserMapper;
import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
@RunWith(MockitoJUnitRunner.class)
public class UserServiceTest {
	@Mock
	private UserMapper userMapper;
	@InjectMocks
	private UserService userService; 
	
	@Test
	public void testGetUserList() {
		List<User> users = new ArrayList<User>();
		
		when(this.userMapper.getAllUserArticles()).thenReturn(users);
		List<User> actual = userService.getAllUserArticles();
		verify(this.userMapper, times(1)).getAllUserArticles();
		
		assertThat(users).isEqualTo(actual);
	}
}
