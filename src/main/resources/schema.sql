CREATE TABLE IF NOT EXISTS todo (
    id IDENTITY
    ,title TEXT NOT NULL
    ,details TEXT
    ,finished BOOLEAN NOT NULL
);

CREATE TABLE IF NOT EXISTS user(
    id IDENTITY
    ,userId TEXT NOT NULL
    ,userName TEXT NOT NULL
    ,gender TEXT NOT NULL
    ,married BOOLEAN
    ,profile TEXT NOT NULL
);