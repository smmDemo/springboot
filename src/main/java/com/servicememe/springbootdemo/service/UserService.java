package com.servicememe.springbootdemo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.servicememe.springbootdemo.domain.User;
import com.servicememe.springbootdemo.mapper.UserMapper;

@Service
public class UserService {
	@Autowired
    private UserMapper dbMapper;

	public List<User> getAllUserArticles() {
		return dbMapper.getAllUserArticles();
	}

	public void addUser(User user) {
		dbMapper.addUser(user);
	}
}
