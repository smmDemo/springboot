package com.servicememe.springbootdemo.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import com.servicememe.springbootdemo.domain.User;

@Mapper
public interface UserMapper {
	@Select("SELECT * FROM user ORDER BY id")
	public List<User> getAllUserArticles();
	
	@Insert("INSERT INTO user (userId,userName,gender,married,profile) VALUES (#{userId},#{userName},#{gender},#{married},#{profile});")
	public void addUser(User user);
}
