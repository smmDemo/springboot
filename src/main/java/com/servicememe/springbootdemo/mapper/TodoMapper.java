package com.servicememe.springbootdemo.mapper;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Select;

import com.servicememe.springbootdemo.domain.Todo;

@Mapper
public interface TodoMapper {

	@Insert("INSERT INTO todo (title, details, finished) VALUES (#{title}, #{details}, #{finished})")
	@Options(useGeneratedKeys = true)
	void insert(Todo todo);

	@Select("SELECT id, title, details, finished FROM todo WHERE id = #{id}")
	Todo select(int id);

}
